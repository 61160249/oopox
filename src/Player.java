
public class Player {
	private char name;
	
	public Player(char name){
		this.name=name;
	}
	
	public char getName() {
		return name;
	}

	
	
	public String toString() {
		return "Player{"+"name="+ name + '}';
		
	};

}
